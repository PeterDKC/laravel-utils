<?php

if (! function_exists('console_line')) {
    /**
     * Returns a separator line for console formatting.
     *
     * @return string
     */
    function console_line()
    {
        return '-------------------';
    }
}
